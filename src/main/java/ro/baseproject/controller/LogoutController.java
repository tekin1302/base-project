package ro.baseproject.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ro.baseproject.enums.Role;

@RestController
@RequestMapping("/rest/logout")
public class LogoutController {

    @PreAuthorize(Role.PERMIT_ALL)
    @RequestMapping(value = "/success", method = RequestMethod.GET)
    public void logout(){
    }
}