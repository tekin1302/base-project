package ro.baseproject.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ro.baseproject.enums.Role;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/")
public class HomeController {

    @PreAuthorize(Role.PERMIT_ALL) // change accordingly
    @RequestMapping(method = RequestMethod.GET)
    public String getHome(HttpServletRequest request, Model model) {
        return "resources/index.html";
    }

}