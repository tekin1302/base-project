package ro.baseproject.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.baseproject.enums.Role;

@RestController
@RequestMapping("/rest/login")
public class LoginController {

    @PreAuthorize(Role.PERMIT_ALL)
    @RequestMapping(value = "/denied")
    public ResponseEntity accessDenied() {
        return new ResponseEntity(HttpStatus.FORBIDDEN);
    }

    @PreAuthorize(Role.PERMIT_ALL)
    @RequestMapping(value = "/needAuth")
    public ResponseEntity needAuth() {
        return new ResponseEntity(HttpStatus.UNAUTHORIZED);
    }

    @PreAuthorize(Role.PERMIT_ALL)
    @RequestMapping(value = "/failed")
    public ResponseEntity failed() {
        return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
    }
}