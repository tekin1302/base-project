package ro.baseproject.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.LazyInitializer;

import java.io.IOException;
import java.io.Serializable;

public class LazyEntitySerializer extends JsonSerializer<HibernateProxy> {

    @Override
    public void serialize(HibernateProxy o, JsonGenerator jgen, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {

        LazyInitializer hibernateLazyInitializer = o.getHibernateLazyInitializer();
        Serializable id = hibernateLazyInitializer.getIdentifier();
        if (o instanceof EntityWithId) {
            EntityWithId entity = (EntityWithId) o;

            jgen.writeStartObject();
            jgen.writeNumberField("id", entity.getId());
            jgen.writeEndObject();
        } else {
            throw new RuntimeException(o.getClass() + " does not extend EntityWithId");
        }
    }
}