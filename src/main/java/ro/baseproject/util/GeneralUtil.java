package ro.baseproject.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class GeneralUtil {
    private static DecimalFormat df2 = new DecimalFormat("0.00");

    public static String stripDecZeros(double d) {
        if(d == (long) d)
            return String.format("%d",(long)d);
        else
            return String.format("%s",d);
    }
    public static String maxDecZeros(BigDecimal d, int zeros) {
        if (zeros == 2) {
//            df2.setRoundingMode(RoundingMode.CEILING);
            return df2.format(d);
        } else {
            StringBuilder trailing = new StringBuilder();
            for (int i = 0; i < zeros; i++) {
                trailing.append("0");
            }
            return new DecimalFormat("0." + trailing.toString()).format(d);
        }
    }

    public static BigDecimal minus(BigDecimal one, BigDecimal two) {
        if (one == null) one = BigDecimal.ZERO;
        if (two == null) two = BigDecimal.ZERO;

        return one.subtract(two);
    }
    public static boolean lower(BigDecimal nr, BigDecimal than) {
        if (nr == null) nr = BigDecimal.ZERO;

        return nr.compareTo(than) < 0;
    }

    public static String removeNomAlphaNumeric(String str) {
        return str.replaceAll("\\W", "");
    }
}
