package ro.baseproject.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.hibernate.Hibernate;
import org.hibernate.collection.internal.PersistentBag;

import java.io.IOException;

public class LazyCollectionSerializer extends JsonSerializer<PersistentBag> {

    @Override
    public void serialize(PersistentBag collection, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
//         do nothing; just ignore the lazy init collections
        jsonGenerator.writeStartArray();
        if (Hibernate.isInitialized(collection)) {
            for (Object obj : collection) {
                jsonGenerator.writeObject(obj);
            }
        }
        jsonGenerator.writeEndArray();
    }
}