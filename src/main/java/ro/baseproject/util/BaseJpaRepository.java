package ro.baseproject.util;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.io.Serializable;

public interface BaseJpaRepository<T, V extends Serializable> extends JpaRepository<T, V>, JpaSpecificationExecutor {
}
