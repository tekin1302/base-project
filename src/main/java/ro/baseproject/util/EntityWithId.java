package ro.baseproject.util;

import javax.persistence.*;

public interface EntityWithId {
    @Access(AccessType.PROPERTY)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_seq")
    @Column(name="id")
    Long getId();
}
