package ro.baseproject.util;

import org.apache.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import ro.baseproject.security.CustomUserDetails;

public class SecurityUtil {

    private static final Logger logger = Logger.getLogger(SecurityUtil.class);

    public static CustomUserDetails getAuthenticatedUser(boolean safe) {
        try {
            if (!SecurityContextHolder.getContext().getAuthentication().getAuthorities().iterator().next().getAuthority().equals("ROLE_ANONYMOUS")) {
                CustomUserDetails principal = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                return principal;
            }
        } catch (Exception e) {
            logger.error("Error while getting authenticated user: " + e.getMessage());
            throw new AccessDeniedException(e.getMessage(), e);
        }
        if (safe) {
            return null;
        } else {
            throw new AccessDeniedException("Not authenticated");
        }
    }

    public static String getSessionId() {
        return RequestContextHolder.currentRequestAttributes().getSessionId();
    }
}
