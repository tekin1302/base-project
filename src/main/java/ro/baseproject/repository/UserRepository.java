package ro.baseproject.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.baseproject.entity.User;
import ro.baseproject.util.BaseJpaRepository;

public interface UserRepository extends BaseJpaRepository<User, Long> {

    @Query("select u from User u where u.username=:username or u.email=:username")
    User findByEmailOrUsername(@Param("username") String username);

}
