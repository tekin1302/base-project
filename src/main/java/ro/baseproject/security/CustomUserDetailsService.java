package ro.baseproject.security;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ro.baseproject.entity.User;
import ro.baseproject.service.UserService;

import javax.servlet.http.HttpServletRequest;

public class CustomUserDetailsService implements UserDetailsService {

    private static final Logger logger = Logger.getLogger(CustomUserDetailsService.class);

    @Autowired
    private UserService userService;

    @Autowired
    private HttpServletRequest request;

    public CustomUserDetails createUserDetails(User user) {
        CustomUserDetails authUser = new CustomUserDetails();
        authUser.setPassword(user.getPassword());
        authUser.setEnabled(true);
        authUser.setUserId(user.getId());
        authUser.setEmail(user.getEmail());
        authUser.setUsername(user.getEmail() != null ? user.getEmail() : user.getUsername());
        authUser.addAuthority(user.getRole().toString());
        authUser.setRole(user.getRole());

        return authUser;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {

        User user = null;
        username = username.trim();

        logger.debug("loadUserByUsername( " + username + ")");
        try {
            user = userService.findForLogin(username);
        } catch (Exception e) {
            logger.error("", e);
        }
        if (user != null && user.isActive()) {
            CustomUserDetails authUser = createUserDetails(user);

            logger.debug("User login");
            return authUser;
        } else {
            logger.debug("Username not found.");
            throw new UsernameNotFoundException("");
        }
    }
}
