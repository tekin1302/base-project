package ro.baseproject.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import ro.baseproject.enums.Role;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class CustomUserDetails implements UserDetails {

    private String email;
    private String username;
    private String password;
    private Long userId;
    private Role role;

    private boolean enabled;
    private Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
    private HashMap<String, Boolean> authoritiesHash;

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /* (non-Javadoc)
     * @see org.springframework.security.core.userdetails.UserDetails#getAuthorities()
     */
    @JsonIgnore
    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    /* (non-Javadoc)
     * @see org.springframework.security.core.userdetails.UserDetails#getPassword()
     */
    @JsonIgnore
    public String getPassword() {
        return password;
    }

    /* (non-Javadoc)
     * @see org.springframework.security.core.userdetails.UserDetails#getUsername()
     */
    public String getUsername() {
        return this.username;
    }

    /* (non-Javadoc)
     * @see org.springframework.security.core.userdetails.UserDetails#isAccountNonExpired()
     */
    @JsonIgnore
    public boolean isAccountNonExpired() {
        // TODO Auto-generated method stub
        return true;
    }

    /* (non-Javadoc)
     * @see org.springframework.security.core.userdetails.UserDetails#isAccountNonLocked()
     */
    @JsonIgnore
    public boolean isAccountNonLocked() {
        // TODO Auto-generated method stub
        return true;
    }

    /* (non-Javadoc)
     * @see org.springframework.security.core.userdetails.UserDetails#isCredentialsNonExpired()
     */
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        // TODO Auto-generated method stub
        return true;
    }

    /* (non-Javadoc)
     * @see org.springframework.security.core.userdetails.UserDetails#isEnabled()
     */
    public boolean isEnabled() {
        return enabled;
    }

    public void addAuthority(String role) {
        GrantedAuthorityImpl gaImpl = new GrantedAuthorityImpl(role);
        authorities.add(gaImpl);

        if (authoritiesHash == null) {
            authoritiesHash = new HashMap<String, Boolean>();
        }
        authoritiesHash.put(role, Boolean.TRUE);
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    private boolean hasAuthority(String authority) {
        Boolean o = authoritiesHash.get(authority);
        return (o != null ? true : false);
    }

    public String getEmail() {
        return email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "AuthenticatedUser [email=" + email + ", password="
                + password + ", userId=" + userId
                + ", enabled=" + enabled + ", authorities=" + authorities
                + ", authoritiesHash=" + authoritiesHash + "]";
    }
}

