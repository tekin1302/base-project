package ro.baseproject.enums;

public enum Role {
    ROLE_ADMIN,
    ROLE_SUPER_ADMIN,
    ROLE_CLIENT;

    public static final String IS_SUPER_ADMIN = "hasRole('ROLE_SUPER_ADMIN')";
    public static final String IS_ADMIN = "hasRole('ROLE_ADMIN')";
    public static final String IS_ADMIN_OR_SUPER_ADMIN = "hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')";
    public static final String IS_AUTHENTICATED = "isAuthenticated()";
    public static final String PERMIT_ALL = "permitAll()";
}
