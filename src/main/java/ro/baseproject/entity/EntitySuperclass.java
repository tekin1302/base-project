package ro.baseproject.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import ro.baseproject.util.EntityWithId;

import javax.persistence.*;
import java.util.Date;

@Access(AccessType.FIELD)
@MappedSuperclass
public class EntitySuperclass implements EntityWithId {

    private Long id;

    @Basic(fetch = FetchType.LAZY)
    @Temporal(value = TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date createDate;

    @Basic(fetch = FetchType.LAZY)
    @JsonIgnore
    @Getter
    @Setter
    private Long createUser;

    @Override
    @Access(AccessType.PROPERTY)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_seq")
    @Column(name="id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EntitySuperclass that = (EntitySuperclass) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ": id= " + this.getId();
    }
}
