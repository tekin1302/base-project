package ro.baseproject.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Email;
import ro.baseproject.enums.Role;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "T_USER")
@Where(clause="deleted = 'false'")
@JsonIgnoreProperties(ignoreUnknown = true)
@SequenceGenerator(name = "pk_seq", sequenceName = "user_seq", allocationSize = 1)
public class User extends EntitySuperclass {

    @Email
    @Size(max = 100)
    @Getter
    @Setter
    private String email;

    // fara getter si setter de la lombok; are json ignore pe setter
    private String password;

    @Enumerated(value = EnumType.STRING)
    @NotNull
    @Getter
    @Setter
    private Role role;

    @Getter
    @Setter
    private String firstName;

    @Getter
    @Setter
    private String lastName;

    @Getter
    @Setter
    private String username;

    @Getter
    @Setter
    private boolean active;

    @Transient
    @Getter
    @Setter
    private String captcha;

    @Transient
    @Getter
    @Setter
    private String confirm;

    @Getter
    @Setter
    private boolean deleted;

    public User() {
    }
    public User(Long id) {
        this.setId(id);
    }

    public User(Long id, String firstName, String lastName, String email) {
        setId(id);
        setFirstName(firstName);
        setLastName(lastName);
        setEmail(email);
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }
}
