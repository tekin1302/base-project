package ro.baseproject.service;

import ro.baseproject.entity.User;

/**
 * Created by tekin on 3/19/2016.
 */
public interface UserService extends BaseService<User> {

    User findForLogin(String username);
}
