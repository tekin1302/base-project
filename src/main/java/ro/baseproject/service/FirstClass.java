package ro.baseproject.service;

import org.springframework.stereotype.Service;
import ro.baseproject.config.AppProperties;

/**
 * Clasa asta se instantiaza inainte de BuffApplicationConfig. In ea se pastreaza valorile din fiserele properties.
 */
@Service(value = "firstClass")
public class FirstClass {
    public FirstClass() {
        AppProperties.getInstance();
    }
}
