package ro.baseproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.baseproject.config.AppProperties;
import ro.baseproject.entity.User;
import ro.baseproject.repository.UserRepository;
import ro.baseproject.util.BaseJpaRepository;

@Service
@Transactional
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {

    @Autowired
    private UserRepository userRepository;

    private AppProperties props = AppProperties.getInstance();

    @Override
    public BaseJpaRepository getRepository() {
        return userRepository;
    }


    @Override
    public User findForLogin(String username) {
        return userRepository.findByEmailOrUsername(username);
    }

}
