package ro.baseproject.service;

import java.util.List;

/**
 * Interfata de service care contine metode comune pentru toate service-urile.
 *
 * Created by tekin on 3/19/2016.
 */
public interface BaseService<T> {

    T getById(Long id);
    void deleteById(Long id);

    void deleteEntity(T entity);

    T save(T entity);
    List<T> save(List<T> entities);

    List<T> getAll();
}
