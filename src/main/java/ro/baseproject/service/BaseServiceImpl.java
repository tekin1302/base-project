package ro.baseproject.service;

import ro.baseproject.entity.EntitySuperclass;
import ro.baseproject.util.BaseJpaRepository;
import ro.baseproject.util.SecurityUtil;

import java.util.Date;
import java.util.List;

/**
 * Created by tekin on 3/19/2016.
 */
public abstract class BaseServiceImpl<T> implements BaseService<T> {

    @Override
    public List<T> getAll() {
        return getRepository().findAll();
    }

    @Override
    public T getById(Long id) {
        return (T)getRepository().findOne(id);
    }

    @Override
    public void deleteById(Long id) {
        getRepository().delete(id);
    }

    @Override
    public void deleteEntity(T entity) {
        getRepository().delete(entity);
    }

    @Override
    public T save(T entity) {
        if (entity instanceof EntitySuperclass) {
            EntitySuperclass tSuperclass = (EntitySuperclass) entity;
            if (tSuperclass.getId() == null) {

                if (tSuperclass.getCreateDate() == null) {
                    tSuperclass.setCreateDate(new Date());
                }
                try {
                    tSuperclass.setCreateUser(SecurityUtil.getAuthenticatedUser(false).getUserId());
                } catch (Exception e) {
                    // ignor eroarea, poate nu e logat
                }
            } else {
                if (tSuperclass.getCreateDate() == null || tSuperclass.getCreateUser() == null) {
                    EntitySuperclass old = (EntitySuperclass) getRepository().findOne(tSuperclass.getId());
                    tSuperclass.setCreateDate(old.getCreateDate());
                    tSuperclass.setCreateUser(old.getCreateUser());
                }
            }
        }
        return (T)getRepository().save(entity);
    }

    @Override
    public List<T> save(List<T> entities) {
       if (entities != null && entities.size() > 0) {
           Long userId = -1L;
           try {
               userId = SecurityUtil.getAuthenticatedUser(false).getUserId();
           } catch (Exception e) {
               // ignor eroarea, poate nu e logat
           }
           for (T entity : entities) {

               if (entity instanceof EntitySuperclass) {

                   EntitySuperclass tSuperclass = (EntitySuperclass) entity;

                   if (tSuperclass.getId() == null) {
                       tSuperclass.setCreateDate(new Date());
                       tSuperclass.setCreateUser(userId);
                   } else {
                       if (tSuperclass.getCreateDate() == null || tSuperclass.getCreateUser() == null) {
                           EntitySuperclass old = (EntitySuperclass) getRepository().findOne(tSuperclass.getId());
                           tSuperclass.setCreateDate(old.getCreateDate());
                           tSuperclass.setCreateUser(old.getCreateUser());
                       }
                   }
               }
           }
           getRepository().save(entities);
       }
        return (List<T>)entities;
    }

    public abstract BaseJpaRepository getRepository();
}
