package ro.baseproject.config;

import org.hibernate.cfg.ImprovedNamingStrategy;
import org.hibernate.internal.util.StringHelper;

/**
 * A custom naming strategy for resolving the column name based on the name of the field
 */
public class JpaNamingStrategy extends ImprovedNamingStrategy {

    @Override
    public String columnName(String columnName) {
        return super.addUnderscores(columnName);
    }

    @Override
    public String propertyToColumnName(String propertyName) {
        return addUnderscores(StringHelper.unqualify(propertyName));
    }

    protected static String addUnderscores(String name) {
        StringBuilder buf = new StringBuilder( name.replace('.', '_') );
        for (int i=1; i<buf.length(); i++) {
            if (
                    Character.isLowerCase(buf.charAt(i - 1)) &&
                            Character.isUpperCase(buf.charAt(i))
                    ) {
                buf.insert(i++, '_');
            }
        }
        return buf.toString().toUpperCase();
    }
}
