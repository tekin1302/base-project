package ro.baseproject.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

/**
 * This is a singleton that contains application properties
 */
public class AppProperties {
    public static final String SPRING_PROFILE = "spring.profiles.active";
    private Logger LOG = LoggerFactory.getLogger(this.getClass());

    private String profile;

    private Properties appProps;

    private AppProperties() {
        this.profile = readProfileFromEnv();

        LOG.debug("Using profile: " + this.profile);
        try {
            appProps = new Properties();
            appProps.load(AppProperties.class.getClassLoader().getResourceAsStream("application-" + profile + ".properties"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * RPropertiesHolder is loaded on the first execution of Singleton.getInstance()
     * or the first access to SingletonHolder.INSTANCE, not before.
     */
    private static class RPropertiesHolder {
        public static final AppProperties INSTANCE = new AppProperties();
    }
    public static AppProperties getInstance() {
        return RPropertiesHolder.INSTANCE;
    }

    /**
     * Read the profile from the vm options.
     * @return Returns the profile, "dev" if not found
     */
    private static String readProfileFromEnv() {
        Object profile = System.getProperties().get(SPRING_PROFILE);
        if (profile != null) {
            return profile.toString();
        }
        return "dev";
    }

    public String getProfile() {
        return profile;
    }

    /**
     * Get a property from the application-<profile>.properties
     * @param key
     * @return Returns the property, null if it does not exist
     */
    public String getProperty(String key) {
        return this.appProps.getProperty(key);
    }


    /**
     * Get a property from the application-<profile>.properties
     * @param key
     * @return Returns the property, throws a RuntimeException if it does not exist
     */
    public String getRequiredProperty(String key) {
        String property = this.appProps.getProperty(key);
        if (property == null) {
            throw new RuntimeException("Property " + key + " not found!");
        }
        return property;
    }

}
