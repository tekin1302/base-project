package ro.baseproject.config;

import liquibase.integration.spring.SpringLiquibase;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.ejb.HibernatePersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Config class for the data source
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "ro.baseproject.repository")
public class DataSourceConfig {

    private Logger LOG = LoggerFactory.getLogger(DataSourceConfig.class);

    private static AppProperties props = AppProperties.getInstance();
    public static final String PERSISTENCE_UNIT_NAME = props.getRequiredProperty("persistenceUnitName");

    /**
     * SpringLiquibase runs the liquibase xml scripts at runtime
     * @return
     */
    @Bean
    public SpringLiquibase liquibase() {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(getConfiguredDataSouce());
        liquibase.setChangeLog("classpath:config/liquibase/master.xml");
        liquibase.setContexts("development,test,production");
        LOG.debug("Configuring Liquibase");

        return liquibase;
    }

    /**
     * Creates a datasource
     * @return
     */
    @Bean(destroyMethod = "")
    public DataSource dataSource() {
        DataSource dataSource = null;
        Connection connection = null;

        try {
            dataSource = getConfiguredDataSouce();
            connection = dataSource.getConnection();
            DatabaseMetaData metaData = connection.getMetaData();

            LOG.debug("DB user: " + metaData.getUserName());
            LOG.debug("DB url: " + metaData.getURL());

        } catch (Exception e) {
            LOG.error("", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOG.error("", e);
                }
            }
        }
        return dataSource;
    }

    /**
     * Gets the datasource from JNDI
     * @return
     */
    private DataSource getConfiguredDataSouce() {

        try {
            Context initCtx = new InitialContext();
            Context envCtx = (Context) initCtx.lookup("java:comp/env");
            DataSource ds = (DataSource)
                    envCtx.lookup("jdbc/baseProject");

            return ds;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Bean
    public JpaTransactionManager transactionManager(LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    public JpaDialect jpaDialect() {
        return new HibernateJpaDialect();
    }

    @Bean
    @DependsOn("liquibase")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {

        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();

        factoryBean.setDataSource(dataSource);
        factoryBean.setPersistenceUnitName(PERSISTENCE_UNIT_NAME);
        factoryBean.setJpaDialect(jpaDialect());
        factoryBean.setPersistenceProviderClass(HibernatePersistence.class);

        Properties jpaProperties = getJpaProperties();
        factoryBean.setJpaProperties(jpaProperties);

        return factoryBean;
    }

    private Properties getJpaProperties() {
        Properties jpaProperties = new Properties();
        jpaProperties.put("hibernate.dialect", props.getRequiredProperty("hibernate.dialect"));
        jpaProperties.put("hibernate.format_sql", props.getRequiredProperty("hibernate.format_sql"));
        jpaProperties.put("hibernate.ejb.naming_strategy", props.getRequiredProperty("hibernate.ejb.naming_strategy"));
        jpaProperties.put("hibernate.show_sql", props.getRequiredProperty("hibernate.show_sql"));
        String hibernateDDL = props.getProperty("hibernate.hbm2ddl.auto");
        if (StringUtils.isNotEmpty(hibernateDDL)) {
            jpaProperties.put("hibernate.hbm2ddl.auto", hibernateDDL);
        }
        return jpaProperties;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

}
